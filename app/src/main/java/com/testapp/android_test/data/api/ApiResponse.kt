package com.testapp.android_test.data.api

import com.google.gson.Gson
import com.google.gson.stream.MalformedJsonException
import com.testapp.android_test.data.models.ApiErrorModel
import com.testapp.android_test.data.models.ApiErrorModel.Companion.DEFAULT_ERROR_RESPONSE_CODE
import com.testapp.android_test.data.models.ApiErrorModel.Companion.INTERNET_ERROR_RESPONSE_CODE
import com.testapp.android_test.data.models.ApiErrorModel.Companion.INVALID_DATA_RESPONSE_CODE
import retrofit2.Response
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

sealed class ApiResponse<T> {

    companion object {

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            val apiErrorModel: ApiErrorModel? = try {
                Gson().fromJson(error.message, ApiErrorModel::class.java)
            } catch (t: Throwable) {
                val errorCode = when (error) {
                    is ConnectException, is UnknownHostException, is TimeoutException -> INTERNET_ERROR_RESPONSE_CODE
                    is MalformedJsonException, is java.lang.NumberFormatException -> INVALID_DATA_RESPONSE_CODE
                    else -> INTERNET_ERROR_RESPONSE_CODE
                }
                ApiErrorModel(statusCode = errorCode)
            }
            return ApiErrorResponse(apiErrorModel ?: ApiErrorModel())
        }

        fun <T> create(
            response: Response<T>,
            gson: Gson
        ): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(
                        body = body
                    )
                }
            } else {
                val msg = response.errorBody()?.string()

                val errorMsg = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }

                val apiErrorModel = try {
                    gson.fromJson(errorMsg, ApiErrorModel::class.java)
                        .copy(
                            statusCode = response.code()
                        )
                } catch (e: Exception) {
                    ApiErrorModel(statusCode = DEFAULT_ERROR_RESPONSE_CODE)
                }
                ApiErrorResponse(apiErrorModel)
            }
        }

    }
}

class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorModel: ApiErrorModel) : ApiResponse<T>()