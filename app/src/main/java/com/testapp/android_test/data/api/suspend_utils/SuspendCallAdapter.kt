package com.testapp.android_test.data.api.suspend_utils

import com.google.gson.Gson
import com.testapp.android_test.data.api.ApiResponse
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type

class SuspendCallAdapter<R>(
    private val responseType: Type,
    private val gson: Gson
) : CallAdapter<R, Deferred<ApiResponse<R>>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<R>): Deferred<ApiResponse<R>> {
        val deferred = CompletableDeferred<ApiResponse<R>>()

        deferred.invokeOnCompletion {
            if (deferred.isCancelled) {
                call.cancel()
            }
        }

        call.enqueue(object : Callback<R> {
            override fun onFailure(call: Call<R>, throwable: Throwable) {
                deferred.complete(ApiResponse.create(throwable))
            }

            override fun onResponse(call: Call<R>, response: Response<R>) {
                deferred.complete(ApiResponse.create(response, gson))
            }
        })

        return deferred
    }
}