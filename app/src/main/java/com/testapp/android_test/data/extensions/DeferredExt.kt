package com.testapp.android_test.data.extensions

import com.testapp.android_test.data.api.ApiErrorResponse
import com.testapp.android_test.data.api.ApiResponse
import com.testapp.android_test.data.api.ApiSuccessResponse
import com.testapp.android_test.data.models.RequestResult
import kotlinx.coroutines.Deferred

suspend fun <R> Deferred<ApiResponse<R>>.awaitResponse(): RequestResult<R> = try {
    when (val response = await()) {
        is ApiSuccessResponse -> RequestResult.Success(response.body)
        is ApiErrorResponse -> RequestResult.ServerError(response.errorModel)
        else -> throw IllegalArgumentException("Unknown response: $response")
    }
} catch (throwable: Throwable) {
    RequestResult.Error(throwable)
}
