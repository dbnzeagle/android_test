package com.testapp.android_test.data.repository

import com.testapp.android_test.data.api.GithubApi
import com.testapp.android_test.data.extensions.awaitResponse
import com.testapp.android_test.data.models.RequestResult
import com.testapp.android_test.data.models.User

class UsersRepositoryImpl(private val githubApi: GithubApi) : UsersRepository {
    override suspend fun getUsersList(offset:Int): RequestResult<List<User>> {
        return githubApi.getUsersAsync(since = offset).awaitResponse()
    }
}