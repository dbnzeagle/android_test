package com.testapp.android_test.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.testapp.android_test.data.api.suspend_utils.SuspendCallAdapterFactory
import com.testapp.android_test.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkService {
    private val myClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .build()

    private fun provideGson(): Gson = GsonBuilder()
        .setLenient()
        .create()

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(myClient)
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(provideGson()))
        .addCallAdapterFactory(SuspendCallAdapterFactory(provideGson()))

        .build()

    fun networkService(): GithubApi = retrofit().create(GithubApi::class.java)
}
