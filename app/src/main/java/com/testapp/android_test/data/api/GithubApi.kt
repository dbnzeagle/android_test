package com.testapp.android_test.data.api

import com.testapp.android_test.data.models.User
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {
    @GET("/users")
    fun getUsersAsync(@Query("since") since: Int): Deferred<ApiResponse<List<User>>>
}