package com.testapp.android_test.data.models

data class ApiErrorModel(
    val statusCode: Int = 0,
    val message: String? = null
){
    companion object {
        const val DEFAULT_ERROR_RESPONSE_CODE = -1
        const val INTERNET_ERROR_RESPONSE_CODE = -2
        const val INVALID_DATA_RESPONSE_CODE = -3
    }
}