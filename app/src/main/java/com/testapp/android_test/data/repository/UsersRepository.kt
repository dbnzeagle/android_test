package com.testapp.android_test.data.repository

import com.testapp.android_test.data.models.RequestResult
import com.testapp.android_test.data.models.User

interface UsersRepository {
    suspend fun getUsersList(offset: Int): RequestResult<List<User>>
}