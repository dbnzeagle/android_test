package com.testapp.android_test.data.models

import com.google.gson.annotations.SerializedName

data class User(
    val login: String,
    val id: Long,
    @SerializedName("avatar_url") val avatarURL: String,
)