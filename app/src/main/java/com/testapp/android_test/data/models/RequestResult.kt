package com.testapp.android_test.data.models

sealed class RequestResult<T> {
    data class Success<T>(val content: T) : RequestResult<T>()
    data class ServerError<T>(val apiError: ApiErrorModel, val data: T? = null) : RequestResult<T>()
    data class Error<T>(val throwable: Throwable) : RequestResult<T>()
}
