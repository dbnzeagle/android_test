package com.testapp.android_test

import android.app.Application
import com.testapp.android_test.data.api.GithubApi
import com.testapp.android_test.data.api.NetworkService
import com.testapp.android_test.data.repository.UsersRepository
import com.testapp.android_test.data.repository.UsersRepositoryImpl

class App : Application() {
    companion object{
        lateinit var networkService: GithubApi
        lateinit var usersRepository: UsersRepository

        fun initNetworkService(network: GithubApi) {
            networkService = network
        }

        fun initRepository(repository: UsersRepository) {
            usersRepository = repository
        }

    }

    override fun onCreate() {
        super.onCreate()
        initNetworkService(NetworkService.networkService())
        initRepository(UsersRepositoryImpl(networkService))
    }
}