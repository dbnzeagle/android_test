package com.testapp.android_test.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListener constructor(private val linearLayoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {
    private val bottomVisibleElement = 1

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = linearLayoutManager.childCount
        val startLoadPosition = linearLayoutManager.itemCount - bottomVisibleElement
        val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

        if (!isLoading() && !isLastPage()) {
            if (visibleItemCount + firstVisibleItemPosition >= startLoadPosition && firstVisibleItemPosition >= 0) {
                onListBottom()
            }
        }
    }

    protected abstract fun onListBottom()

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean
}