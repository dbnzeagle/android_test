package com.testapp.android_test.ui.users.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.testapp.android_test.data.models.User

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<UsersItem>() {
    override fun areItemsTheSame(
        oldItem: UsersItem,
        newItem: UsersItem
    ): Boolean = when {
        oldItem is UsersItem.CommonUser && newItem is UsersItem.CommonUser ->
            oldItem.user.id == newItem.user.id

        else -> oldItem::class == newItem::class
    }

    override fun areContentsTheSame(
        oldItem: UsersItem,
        newItem: UsersItem
    ): Boolean = when {
        oldItem is UsersItem.CommonUser && newItem is UsersItem.CommonUser ->
            oldItem.user.id == newItem.user.id

        else -> oldItem::class == newItem::class
    }
}


class UsersAdapter(onItemClicked: (User) -> Unit) :
    AsyncListDifferDelegationAdapter<UsersItem>(DIFF_CALLBACK) {
    init {
        delegatesManager
            .addDelegate(usersDelegate(onItemClicked::invoke))
    }
}