package com.testapp.android_test.ui.users

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.testapp.android_test.R
import com.testapp.android_test.data.models.User
import com.testapp.android_test.databinding.UsersFragmentBinding
import com.testapp.android_test.ui.user_details.UserDetailsFragment
import com.testapp.android_test.ui.users.adapter.UsersAdapter

class UsersFragment : Fragment(R.layout.users_fragment) {

    private lateinit var usersViewModel: UsersViewModel
    private var _binding: UsersFragmentBinding? = null
    private val binding get() = _binding!!

    private val usersAdapter by lazy {
        UsersAdapter(
            onItemClicked = ::navigateToDetails
        )
    }

    private fun navigateToDetails(user: User) {
        parentFragmentManager.commit {
            replace(R.id.fragmentContainer, UserDetailsFragment.newInstance(user))
            addToBackStack(UsersFragment::class.java.simpleName)

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = UsersFragmentBinding.bind(view)
        _binding = binding
        usersViewModel = ViewModelProvider(this).get(UsersViewModel::class.java)
        initViews()
    }

    private fun initViews() {
        val linearLayoutManager = LinearLayoutManager(context)
        binding.usersList.apply {
            adapter = usersAdapter
            layoutManager = linearLayoutManager
        }
        binding.usersProgress.visibility = View.VISIBLE
        binding.usersList.visibility = View.GONE
        usersViewModel.usersList.observe(viewLifecycleOwner) {
            usersAdapter.items = it
            binding.usersList.visibility = View.VISIBLE
        }
        usersViewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
        usersViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.usersProgress.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    companion object {
        fun newInstance() =
            UsersFragment()
    }
}