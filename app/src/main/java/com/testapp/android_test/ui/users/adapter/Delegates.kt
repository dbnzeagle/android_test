package com.testapp.android_test.ui.users.adapter

import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.testapp.android_test.data.models.User
import com.testapp.android_test.databinding.UsersItemBinding

internal fun usersDelegate(onItemClicked: (User) -> Unit) =
    adapterDelegateViewBinding<UsersItem.CommonUser,
            UsersItem, UsersItemBinding>({ layoutInflater, parent ->
        UsersItemBinding.inflate(
            layoutInflater,
            parent,
            false
        )
    }) {
        binding.usersItemRoot.setOnClickListener { onItemClicked.invoke(item.user) }

        bind {
            binding.usersName.text = item.user.login
            binding.usersId.text = item.user.id.toString()
            Glide.with(itemView).load(item.user.avatarURL).into(binding.usersImage)
        }
    }
