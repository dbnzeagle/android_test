package com.testapp.android_test.ui.user_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.testapp.android_test.R
import com.testapp.android_test.data.models.User
import com.testapp.android_test.databinding.UserDetailsFragmentBinding
import com.testapp.android_test.utils.putArgs

class UserDetailsFragment : Fragment(R.layout.user_details_fragment) {

    private var _binding: UserDetailsFragmentBinding? = null
    private val binding get() = _binding!!

    val user: User? by lazy {
        Gson().fromJson(requireArguments().getString(USER), User::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = UserDetailsFragmentBinding.bind(view)
        _binding = binding
        initViews()
    }

    private fun initViews() {
        binding.apply {
            userDetailsText.text = user?.login
            Glide.with(this@UserDetailsFragment).load(user?.avatarURL).into(userDetailsImage)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        fun newInstance(user: User) = UserDetailsFragment().putArgs {
            putString(USER, Gson().toJson(user))
        }

        private const val USER = "USER"
    }

}