package com.testapp.android_test.ui.users.adapter

import com.testapp.android_test.data.models.User

sealed class UsersItem {
    data class CommonUser(
        val user: User
    ) : UsersItem()

}