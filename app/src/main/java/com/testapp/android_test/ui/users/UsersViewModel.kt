package com.testapp.android_test.ui.users

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.testapp.android_test.App
import com.testapp.android_test.data.models.RequestResult
import com.testapp.android_test.data.models.User
import com.testapp.android_test.ui.users.adapter.UsersItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UsersViewModel : ViewModel() {
    val usersList: MutableLiveData<List<UsersItem.CommonUser>> = MutableLiveData(emptyList())
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(true)

    init {
        fetchUsersList()
    }

    fun fetchUsersList() {
        viewModelScope.launch(Dispatchers.IO) {
            when (val response = App.usersRepository.getUsersList(0)) {
                is RequestResult.Success -> handleSuccess(response.content)
                is RequestResult.ServerError -> handleError(response.apiError.message)
                is RequestResult.Error -> handleError(response.throwable.message)
            }
        }
    }

    private fun handleSuccess(users: List<User>) {
        isLoading.postValue(false)
        val commonUsers = users.map { UsersItem.CommonUser(it) }
        usersList.postValue(commonUsers)
    }

    private fun handleError(error: String?) {
        isLoading.postValue(false)
        errorMessage.postValue(error)
    }

}